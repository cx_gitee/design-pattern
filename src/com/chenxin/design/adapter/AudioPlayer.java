package com.chenxin.design.adapter;

import com.chenxin.design.adapter.ext.MediaPlayerAdapter;

/**
 *  支持mp3
 *  如果后续这个要支持更多的播放比如mp4, vlc模式的怎么办
 *  1、要么增加其他的AudioPlayer实现MediaPlayer，这样就会有多份实现
 *  2、要么就加一个适配器，放在现有的AudioPlayer中，后续不管添加什么，只需要增加适配器就可以
 *  所以新增一个高级适配器
 */
public class AudioPlayer implements MediaPlayer {

    private MediaPlayerAdapter mediaAdapter;

    /**
     * 1.1日需求
     * @param audioType
     * @param fileName
     */
    @Override
    public void play(String audioType, String fileName) {
        //第一版本需求只提供mp3播放
        if("mp3".equalsIgnoreCase(audioType)){
            System.out.println("Playing mp3 file: name=" + fileName);
        }

        //mediaAdapter 提供了播放其他文件格式的支持
        else if(audioType.equalsIgnoreCase("vlc")
                || audioType.equalsIgnoreCase("mp4")){
            mediaAdapter = new MediaPlayerAdapter(audioType);
            mediaAdapter.play(audioType, fileName);
        }
        else{
            System.out.println("Invalid media. "+
                    audioType + " format not supported");
        }
    }
}
