package com.chenxin.design.adapter.ext;

import com.chenxin.design.adapter.MediaPlayer;

/**
 * 增加一个媒体播放器的Adapter适配器
 * 来适配后续不同的播放格式
 */
public class MediaPlayerAdapter implements MediaPlayer {

    private AdvancedMediaPlayer advancedMediaPlayer;

    /**
     * 构造方法接收传参，实例化不同的对象
     * @param audioType
     */
    public MediaPlayerAdapter(String audioType) {
        if(audioType.equalsIgnoreCase("vlc")){
            advancedMediaPlayer = new VlcPlayer();
        }
        if(audioType.equalsIgnoreCase("mp4")){
            advancedMediaPlayer = new Mp4Player();
        }
    }

    @Override
    public void play(String audioType, String fileName) {
        if (audioType.equalsIgnoreCase("vlc")) {
            advancedMediaPlayer.playVlc(fileName);
        } else if (audioType.equalsIgnoreCase("mp4")) {
            advancedMediaPlayer.playMp4(fileName);
        }
    }
}
