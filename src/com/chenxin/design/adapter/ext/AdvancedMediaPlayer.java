package com.chenxin.design.adapter.ext;

/**
 * 高级播放适配器适配
 * 后期如果要支持不同的播放
 */
public interface AdvancedMediaPlayer {
   public void playVlc(String fileName);
   public void playMp4(String fileName);
}