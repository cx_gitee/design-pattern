package com.chenxin.design.adapter;

/**
 * 媒体播放接口，AudioPlayer可以播放mp3的音频文件
 * 1.1日需求
 */
public interface MediaPlayer {
   public void play(String audioType, String fileName);
}