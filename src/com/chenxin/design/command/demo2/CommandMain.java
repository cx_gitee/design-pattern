package com.chenxin.design.command.demo2;
/**
 * @Description: 充当Client
 * @title: CommandMain
 * @Author Star_Chen
 * @Date: 2021/12/20 23:43
 * @Version 1.0
 */
public class CommandMain {

    public static void main(String[] args) {
        Television tv = new Television();

        RemoteControl remoteControl = new RemoteControl();
        remoteControl.switchTV(new CCTV1Command(tv));
        remoteControl.switchTV(new CCTV2Command(tv));
        remoteControl.switchTV(new CCTV3Command(tv));
        //切换上一个台
        remoteControl.back();
    }
}
