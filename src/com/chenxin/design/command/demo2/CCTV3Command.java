package com.chenxin.design.command.demo2;

/**
 * CCTV1的执行命令
 */
public class CCTV3Command extends Command {

    public CCTV3Command(Television television) {
        super(television);
    }

    @Override
    void execute() {
        television.playCctv3();
    }
}
