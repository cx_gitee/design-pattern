package com.chenxin.design.command.demo2;

/**
 * @Description: 每个请求封装成的命令对象，客户端只需要调用不同的命令就可以达到将请求到不用的方法上
 * @title: Command
 * @Author Star_Chen
 * @Date: 2021/12/20 23:36
 * @Version 1.0
 */
public abstract class Command {

    /**
     * protected修饰的对象或者方法，子类
     * 1、父类的protected成员是在同一个包内可见的，并且对子类可见
     * 2、若子类与父类类不在同一包中，假如在两个不同的包里，那么在子类中，子类实例可以访问其从父类继承而来的protected方法，而不能访问父类实例（new出来的）的protected方法
     */
    protected Television television;

    /**
     * @Date: 2021/12/20 23:36
     * @Description: 抽象类的构造方法，是给子类使用的；
     */
    public Command(Television television) {
        this.television = television;
    }

    /**
     * @Date: 2021/12/20 23:37
     * @Description: 抽象的命令执行方法
     */
    abstract void execute();
}
