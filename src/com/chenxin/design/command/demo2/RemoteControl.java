package com.chenxin.design.command.demo2;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 遥控器，充当invoker
 * @title: RemoteControl
 * @Author Star_Chen
 * @Date: 2021/12/20 23:35
 * @Version 1.0
 */
public class RemoteControl {

    List<Command> historyCommand = new ArrayList<Command>();

    /**
     * @Date: 2021/12/20 23:40
     * @Description: 切换卫视
     */
    public void switchTV(Command command) {
        historyCommand.add(command);
        command.execute();
    }

    /**
     * @Date: 2021/12/20 23:39
     * @Description: 遥控器返回命令
     */
    public void back() {
        if (historyCommand.isEmpty()) {
            return;
        }
        int size = historyCommand.size();
        int preIndex = size - 2 <= 0 ? 0 : size - 2;

        //获取上一个播放某卫视的命令
        Command preCommand = historyCommand.remove(preIndex);

        preCommand.execute();
    }

}
