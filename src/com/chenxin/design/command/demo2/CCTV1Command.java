package com.chenxin.design.command.demo2;

/**
 * @Description: CCTV1的执行命令
 * @title: CCTV1Command
 * @Author Star_Chen
 * @Date: 2021/12/20 23:19
 * @Version 1.0
 */
public class CCTV1Command extends Command {

    /**
     * @Date: 2021/12/20 23:20
     * @Description: 构造传入
     */
    public CCTV1Command(Television television) {
        super(television);
    }

    /**
     * @Date: 2021/12/20 23:20
     * @Description:
     */
    @Override
    void execute() {
        television.playCctv1();
    }
}
