package com.chenxin.design.command.demo2;

/**
 * CCTV1的执行命令
 */
public class CCTV2Command extends Command {

    public CCTV2Command(Television television) {
        super(television);
    }

    @Override
    void execute() {
        television.playCctv2();
    }
}
