package com.chenxin.design.command.demo2;

/**
 * @Description: 电视机类充当Receiver，命令的接受者对象
 * @title: Television
 * @Author Star_Chen
 * @Date: 2021/12/20 23:35
 * @Version 1.0
 */
public class Television {

    public void playCctv1() {
        System.out.println("--CCTV1--");
    }

    public void playCctv2() {
        System.out.println("--CCTV2--");
    }

    public void playCctv3() {
        System.out.println("--CCTV3--");
    }

    public void playCctv4() {
        System.out.println("--CCTV4--");
    }

    public void playCctv5() {
        System.out.println("--CCTV5--");
    }

    public void playCctv6() {
        System.out.println("--CCTV6--");
    }
}
