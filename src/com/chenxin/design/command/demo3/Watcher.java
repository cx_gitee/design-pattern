package com.chenxin.design.command.demo3;

/**
 * @Description:
 * @title: Watcher
 * @Author Star_Chen
 * @Date: 2021/12/20 23:22
 * @Version 1.0
 */
public class Watcher {

    /**
     * @Date: 2021/12/20 23:23
     * @Description: 持有Television
     */
    public Television tv;

    public Watcher(Television tv) {
        this.tv = tv;
    }

    public void playCCTV1() {
        tv.playCCTV1();
    }

    public void playCCTV2() {
        tv.playCCTV2();
    }

    public void playCCTV3() {
        tv.playCCTV3();
    }

    public void playCCTV4() {
        tv.playCCTV4();
    }
}
