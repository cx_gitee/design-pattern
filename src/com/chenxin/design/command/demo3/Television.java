package com.chenxin.design.command.demo3;

/**
 * @Description:
 * @title: Television
 * @Author Star_Chen
 * @Date: 2021/12/20 23:21
 * @Version 1.0
 */
public class Television {

    public void playCCTV1() {
        System.out.println("--CCTV1--");
    }

    public void playCCTV2() {
        System.out.println("--CCTV2--");
    }

    public void playCCTV3() {
        System.out.println("--CCTV3--");
    }

    public void playCCTV4() {
        System.out.println("--CCTV4--");
    }
}
