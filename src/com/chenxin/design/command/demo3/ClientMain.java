package com.chenxin.design.command.demo3;

/**
 * @Description:
 * @title: ClientMain
 * @Author Star_Chen
 * @Date: 2021/12/20 23:23
 * @Version 1.0
 */
public class ClientMain {

    public static void main(String[] args) {
        Watcher watcher = new Watcher(new Television());
        watcher.playCCTV1();
        watcher.playCCTV2();
        watcher.playCCTV3();
        watcher.playCCTV4();

        //回退到CCTV1
        watcher.playCCTV3();
        watcher.playCCTV2();
        watcher.playCCTV1();
    }
}
