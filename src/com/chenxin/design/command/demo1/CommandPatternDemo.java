package com.chenxin.design.command.demo1;

public class CommandPatternDemo {

    public static void main(String[] args) {
        //类比成电视机，里面可以有很多很多的执行方法
        Stock stock = new Stock();

        //于是相关联的业务对象先出来，有买的业务，有卖的业务，我要把这个命令拿过来剖析下看看是不是我这边的，所以需要个构造接收下
        BuyRequest buyRequest = new BuyRequest(stock);
        SellRequest sellRequest = new SellRequest(stock);

        //区分开了后，交给执行器去添加这些命令放到队列里
        Broker broker = new Broker();
        broker.takeOrder(buyRequest);
        broker.takeOrder(sellRequest);

        //这些命令执行下去
        broker.placeOrder();
    }
}
