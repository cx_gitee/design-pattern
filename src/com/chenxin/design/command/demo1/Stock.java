package com.chenxin.design.command.demo1;

/**
 * 请求的 Stock 类
 */
public class Stock {

    private String name = "ABC";
    private int quantity = 10;

   /**
    * 买的请求
    */
   public void buy() {
        System.out.println("Stock [ Name: " + name + ", Quantity: " + quantity + " ] bought");
    }

   /**
    * 卖的请求
    */
   public void sell() {
        System.out.println("Stock [ Name: " + name + ", Quantity: " + quantity + " ] sold");
    }
}