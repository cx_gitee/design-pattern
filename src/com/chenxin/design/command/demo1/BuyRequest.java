package com.chenxin.design.command.demo1;

/**
 * 买的请求命令
 */
public class BuyRequest implements Order {

    Stock stock;

    public BuyRequest(Stock stock) {
        this.stock = stock;
    }


    @Override
    public void execute() {
        stock.buy();
    }
}
