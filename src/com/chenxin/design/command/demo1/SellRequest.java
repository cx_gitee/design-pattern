package com.chenxin.design.command.demo1;

/**
 * 卖的请求
 */
public class SellRequest implements Order {
   private Stock stock;
 
   public SellRequest(Stock stock){
      this.stock = stock;
   }
 
   @Override
   public void execute() {
      stock.sell();
   }
}