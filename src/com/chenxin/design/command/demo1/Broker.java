package com.chenxin.design.command.demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * 具体的执行命令的类
 */
public class Broker {

    private List<Order> orderList = new ArrayList<>();

    /**
     * 取出命令
     * @param order
     */
    public void takeOrder(Order order){
        orderList.add(order);
    }

    /**
     * 执行命令的方法
     */
    public void placeOrder(){
        for (Order order : orderList) {
            order.execute();
        }
        orderList.clear();
    }

}
