package com.chenxin.design.command.demo1;

/**
 * 创建命令接口
 */
public interface Order {
   /**
    * 抽象执行方法
    */
   void execute();
}