package com.chenxin.design.strategy.demo2;

/**
 * @Description: geigei的叫
 * @title: UglyQuackBehavior
 * @Author Star_Chen
 * @Date: 2021/12/22 0:01
 * @Version 1.0
 */
public class UglyQuackBehavior implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("geigei的叫！");
    }
}
