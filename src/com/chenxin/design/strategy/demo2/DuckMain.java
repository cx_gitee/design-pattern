package com.chenxin.design.strategy.demo2;

/**
 * @Description:
 * @title: DuckMain
 * @Author Star_Chen
 * @Date: 2021/12/21 22:44
 * @Version 1.0
 */
public class DuckMain {

    public static void main(String[] args) {

/*        RedDuck redDuck = new RedDuck();
        BlackDuck blackDuck = new BlackDuck();

        redDuck.quack();
        redDuck.perform();
        redDuck.swim();
        System.out.println("<==========================>");
        blackDuck.quack();
        blackDuck.perform();
        blackDuck.swim();*/

        //父类为Duck，屏蔽了超类的差别性
        Duck redDuck = new RedDuck();
        Duck blackDuck = new BlackDuck();

        redDuck.perform();
        redDuck.setFlyBehavior(new FlyBehavior(){

            @Override
            public void fly() {
                System.out.println("红鸭子我不飞了");
            }
        });
        redDuck.fly();

        System.out.println("<==========================>");

        blackDuck.perform();
        blackDuck.setQuackBehavior(new QuackBehavior() {
            @Override
            public void quack() {
                System.out.println("黑鸭子我不叫了");
            }
        });
        blackDuck.quack();
    }
}
