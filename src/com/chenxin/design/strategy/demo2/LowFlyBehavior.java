package com.chenxin.design.strategy.demo2;

/**
 * @Description: 低飞
 * @title: LowFlyBehavior
 * @Author Star_Chen
 * @Date: 2021/12/21 23:55
 * @Version 1.0
 */
public class LowFlyBehavior implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("鸭子会低飞！");
    }
}
