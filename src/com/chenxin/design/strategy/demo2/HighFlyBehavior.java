package com.chenxin.design.strategy.demo2;

/**
 * @Description: 高飞
 * @title: HighFlyBehavior
 * @Author Star_Chen
 * @Date: 2021/12/21 23:54
 * @Version 1.0
 */
public class HighFlyBehavior implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("鸭子会高飞！");
    }
}
