package com.chenxin.design.strategy.demo2;

/**
 * @Description: 红鸭子
 * @title: RedDuck
 * @Author Star_Chen
 * @Date: 2021/12/21 22:40
 * @Version 1.0
 */
public class RedDuck extends Duck {

    public RedDuck() {
        flyBehavior = new HighFlyBehavior();
        quackBehavior = new UglyQuackBehavior();
    }

    @Override
    public void perform() {
        System.out.println("我是红色的鸭子！");
    }
}
