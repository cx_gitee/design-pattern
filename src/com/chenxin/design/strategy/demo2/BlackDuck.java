package com.chenxin.design.strategy.demo2;

/**
 * @Description: 黑鸭子
 * @title: BlackDuck
 * @Author Star_Chen
 * @Date: 2021/12/21 22:41
 * @Version 1.0
 */
public class BlackDuck extends Duck {

    public BlackDuck() {
        flyBehavior = new LowFlyBehavior();
        quackBehavior = new GiaoQuackBehavior();
    }

    @Override
    public void perform() {
        System.out.println("我是黑色的鸭子！");
    }
}
