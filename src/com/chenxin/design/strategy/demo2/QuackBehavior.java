package com.chenxin.design.strategy.demo2;

/**
 * @Description:
 * @title: QuackBehavior
 * @Author Star_Chen
 * @Date: 2021/12/22 0:00
 * @Version 1.0
 */
public interface QuackBehavior {

    /**
     * @Date: 2021/12/22 0:01
     * @Description: 叫的行为
     */
    void quack();
}
