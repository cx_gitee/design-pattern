package com.chenxin.design.strategy.demo2;

/**
 * @Description:
 * @title: FlyBehavior
 * @Author Star_Chen
 * @Date: 2021/12/21 23:53
 * @Version 1.0
 */
public interface FlyBehavior {

    /**
     * @Date: 2021/12/21 23:54
     * @Description: 飞的行为
     */
    void fly();
}
