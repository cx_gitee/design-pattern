package com.chenxin.design.strategy.demo2;

/**
 * @Description: 设计一个抽象类鸭子
 * @title: Duck
 * @Author Star_Chen
 * @Date: 2021/12/21 22:36
 * @Version 1.0
 */
public abstract class Duck {

    //具备抽象行为
    FlyBehavior flyBehavior;

    QuackBehavior quackBehavior;

    /**
     * @Date: 2021/12/22 0:07
     * @Description: 实例化对象时可以动态的改变对象的行为(比继承灵活性强)
     */
    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    /**
     * @Date: 2021/12/22 0:07
     * @Description: 实例化对象时可以动态的改变对象的行为(比继承灵活性强)
     */
    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }

    /**
     * @Date: 2021/12/21 22:46
     * @Description: 本抽象父类中自己实现叫的方法，子类也可以自己实现
     */
    public void quack(){
//        System.out.println("我是鸭子，我会叫呱呱呱！");
        quackBehavior.quack();
    }

    public void fly(){
        flyBehavior.fly();
    }

    /**
     * @Date: 2021/12/21 22:39
     * @Description: 抽象的自我介绍颜色方法让子类实现
     */
    public abstract void perform();

    /**
     * @Date: 2021/12/21 22:39
     * @Description: 本抽象父类中自己实现游泳方法，子类也可以自己实现
     */
    public void swim(){
        System.out.println("我是个鸭子，会游泳~");
    }
}
