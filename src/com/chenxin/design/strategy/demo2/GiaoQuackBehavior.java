package com.chenxin.design.strategy.demo2;

/**
 * @Description: giaogiao的叫
 * @title: GiaoQuackBehavior
 * @Author Star_Chen
 * @Date: 2021/12/22 0:01
 * @Version 1.0
 */
public class GiaoQuackBehavior implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("一给我里giaogiao的叫！");
    }
}
