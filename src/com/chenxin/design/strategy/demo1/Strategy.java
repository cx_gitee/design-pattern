package com.chenxin.design.strategy.demo1;

/**
 * 策略类
 */
public interface Strategy {
   public int doOperation(int num1, int num2);
}